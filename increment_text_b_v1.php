<?php
#Name:Increment Text B v1
#Description:Increment text by 1 valid increment using content generation techniques. Returns text as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Only characters "a-zA-Z,;:.?!'"()[]/-0-9&%$@" are used or supported.
#Arguments:'string' is a string containing the text to increment. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('increment_text_b_v1') === FALSE){
function increment_text_b_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	} else if (preg_replace('/([a-z]|[A-Z]|[0-9]|,|;|:|\.|\?|!|\'|"|\(|\)|\[|\]|\/|-|&|%|\$|@)/', '', $string) != ''){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Break into an array of single characters. Reverse the array so that increment can be done from end to start of string.
		$string = @mb_str_split($string, 1);
		$string = @array_reverse($string);
		###Increment the first character between "a-zA-Z,;:.?!'"()[]/-0-9&%$@". When it exceeds "@" it loops back to "a", and the next value is incremented by 1. If a next doesn't exist create it as "a".
		foreach ($string as &$value) {
			if ($value === 'a'){
				$value = 'b';
				goto break_increment;
			} else if ($value === 'b'){
				$value = 'c';
				goto break_increment;
			} else if ($value === 'c'){
				$value = 'd';
				goto break_increment;
			} else if ($value === 'd'){
				$value = 'e';
				goto break_increment;
			} else if ($value === 'e'){
				$value = 'f';
				goto break_increment;
			} else if ($value === 'f'){
				$value = 'g';
				goto break_increment;
			} else if ($value === 'g'){
				$value = 'h';
				goto break_increment;
			} else if ($value === 'h'){
				$value = 'i';
				goto break_increment;
			} else if ($value === 'i'){
				$value = 'j';
				goto break_increment;
			} else if ($value === 'j'){
				$value = 'k';
				goto break_increment;
			} else if ($value === 'k'){
				$value = 'l';
				goto break_increment;
			} else if ($value === 'l'){
				$value = 'm';
				goto break_increment;
			} else if ($value === 'm'){
				$value = 'n';
				goto break_increment;
			} else if ($value === 'n'){
				$value = 'o';
				goto break_increment;
			} else if ($value === 'o'){
				$value = 'p';
				goto break_increment;
			} else if ($value === 'p'){
				$value = 'q';
				goto break_increment;
			} else if ($value === 'q'){
				$value = 'r';
				goto break_increment;
			} else if ($value === 'r'){
				$value = 's';
				goto break_increment;
			} else if ($value === 's'){
				$value = 't';
				goto break_increment;
			} else if ($value === 't'){
				$value = 'u';
				goto break_increment;
			} else if ($value === 'u'){
				$value = 'v';
				goto break_increment;
			} else if ($value === 'v'){
				$value = 'w';
				goto break_increment;
			} else if ($value === 'w'){
				$value = 'x';
				goto break_increment;
			} else if ($value === 'x'){
				$value = 'y';
				goto break_increment;
			} else if ($value === 'y'){
				$value = 'z';
				goto break_increment;
			} else if ($value === 'z'){
				$value = 'A';
				goto break_increment;
			} else if ($value === 'A'){
				$value = 'B';
				goto break_increment;
			} else if ($value === 'B'){
				$value = 'C';
				goto break_increment;
			} else if ($value === 'C'){
				$value = 'D';
				goto break_increment;
			} else if ($value === 'D'){
				$value = 'E';
				goto break_increment;
			} else if ($value === 'E'){
				$value = 'F';
				goto break_increment;
			} else if ($value === 'F'){
				$value = 'G';
				goto break_increment;
			} else if ($value === 'G'){
				$value = 'H';
				goto break_increment;
			} else if ($value === 'H'){
				$value = 'I';
				goto break_increment;
			} else if ($value === 'I'){
				$value = 'J';
				goto break_increment;
			} else if ($value === 'J'){
				$value = 'K';
				goto break_increment;
			} else if ($value === 'K'){
				$value = 'L';
				goto break_increment;
			} else if ($value === 'L'){
				$value = 'M';
				goto break_increment;
			} else if ($value === 'M'){
				$value = 'N';
				goto break_increment;
			} else if ($value === 'N'){
				$value = 'O';
				goto break_increment;
			} else if ($value === 'O'){
				$value = 'P';
				goto break_increment;
			} else if ($value === 'P'){
				$value = 'Q';
				goto break_increment;
			} else if ($value === 'Q'){
				$value = 'R';
				goto break_increment;
			} else if ($value === 'R'){
				$value = 'S';
				goto break_increment;
			} else if ($value === 'S'){
				$value = 'T';
				goto break_increment;
			} else if ($value === 'T'){
				$value = 'U';
				goto break_increment;
			} else if ($value === 'U'){
				$value = 'V';
				goto break_increment;
			} else if ($value === 'V'){
				$value = 'W';
				goto break_increment;
			} else if ($value === 'W'){
				$value = 'X';
				goto break_increment;
			} else if ($value === 'X'){
				$value = 'Y';
				goto break_increment;
			} else if ($value === 'Y'){
				$value = 'Z';
				goto break_increment;
			} else if ($value === 'Z'){
				$value = ',';
				goto break_increment;
			} else if ($value === ','){
				$value = ';';
				goto break_increment;
			} else if ($value === ';'){
				$value = ':';
				goto break_increment;
			} else if ($value === ':'){
				$value = '.';
				goto break_increment;
			} else if ($value === '.'){
				$value = '?';
				goto break_increment;
			} else if ($value === '?'){
				$value = '!';
				goto break_increment;
			} else if ($value === '!'){
				$value = '\'';
				goto break_increment;
			} else if ($value === '\''){
				$value = '"';
				goto break_increment;
			} else if ($value === '"'){
				$value = '(';
				goto break_increment;
			} else if ($value === '('){
				$value = ')';
				goto break_increment;
			} else if ($value === ')'){
				$value = '[';
				goto break_increment;
			} else if ($value === '['){
				$value = ']';
				goto break_increment;
			} else if ($value === ']'){
				$value = '/';
				goto break_increment;
			} else if ($value === '/'){
				$value = '-';
				goto break_increment;
			} else if ($value === '-'){
				$value = '0';
				goto break_increment;
			} else if ($value === '0'){
				$value = '1';
				goto break_increment;
			} else if ($value === '1'){
				$value = '2';
				goto break_increment;
			} else if ($value === '2'){
				$value = '3';
				goto break_increment;
			} else if ($value === '3'){
				$value = '4';
				goto break_increment;
			} else if ($value === '4'){
				$value = '5';
				goto break_increment;
			} else if ($value === '5'){
				$value = '6';
				goto break_increment;
			} else if ($value === '6'){
				$value = '7';
				goto break_increment;
			} else if ($value === '7'){
				$value = '8';
				goto break_increment;
			} else if ($value === '8'){
				$value = '9';
				goto break_increment;
			} else if ($value === '9'){
				$value = "&";
				goto break_increment;
			} else if ($value === '&'){
				$value = '%';
				goto break_increment;
			} else if ($value === '%'){
				$value = '$';
				goto break_increment;
			} else if ($value === '$'){
				$value = '@';
				goto break_increment;
			} else {
				$value = 'a';
			}
		}
		unset($value);
		break_increment:
		###Unreverse array
		$string = @array_reverse($string);
		###Check if entire array is 'a's, and if it is append 'a' to the end to increment to next range.
		$check = FALSE;
		foreach ($string as &$value) {
			if ($value !== 'a'){
				$check = TRUE;
			}
		}
		unset($value);
		if ($check === FALSE){
			$string[] = 'a';
		}
		###Convert array to string
		$string = @implode($string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('increment_text_b_v1_format_error') === FALSE){
				function increment_text_b_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("increment_text_b_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>